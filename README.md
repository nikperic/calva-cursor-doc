
Adapted from [Calva](https://github.com/BetterThanTomorrow/calva). All credits go there

This repo exists purely as a means to have a standalone cursor-doc package.


To compile:

```
npm install
tsc
```

Generates files in out folder


Copied files from [Calva](https://github.com/BetterThanTomorrow/calva/tree/master/src), specifically the `cursor-doc` folder.